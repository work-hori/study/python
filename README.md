python
===

## Description
A project that introduces programs abount python.

## Clone
### SSH

### HTTPS


## Directory tree
<pre>
.
├── AI_framework
│   └── pytorch
│       └── tutorial
│           ├── README.md
│           ├── __pycache__
│           │   ├── datasetloader.cpython-39.pyc
│           │   └── models.cpython-39.pyc
│           ├── datasetloader.py
│           ├── mlp.py
│           ├── models.py
│           └── requirements.txt
├── README.md
├── Roboter
│   ├── README.md
│   ├── main.py
│   ├── roboter
│   │   ├── __init__.py
│   │   ├── controller
│   │   │   ├── __init__.py
│   │   │   └── conversation.py
│   │   ├── models
│   │   │   ├── __init__.py
│   │   │   ├── ranking.py
│   │   │   └── robot.py
│   │   ├── templates
│   │   │   ├── good_by.txt
│   │   │   ├── greeting.txt
│   │   │   ├── hello.txt
│   │   │   └── which_restaurant.txt
│   │   └── views
│   │       ├── __init__.py
│   │       └── console.py
│   ├── setup.cfg
│   └── setup.py
└── create_readme.sh

10 directories, 25 files
</pre>

## Author
Kyosuke Hori
kyosuke1117sucre@gmail.com
