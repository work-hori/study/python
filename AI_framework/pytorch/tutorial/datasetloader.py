from torchvision import datasets, transforms
from torch.utils.data import DataLoader


class DatasetLoader(object):

    def MNIST(root="./data", train=True, download=True,
              num_batches=100, shuffle=True):
        transform = transforms.Compose([
            transforms.ToTensor()
        ])
        dataset = datasets.MNIST(root=root, train=train, download=download,
                                 transform=transform)
        dataloader = DataLoader(dataset, batch_size=num_batches,
                                shuffle=shuffle)
        return dataloader, num_batches
