import os
import argparse
import datetime
import time

import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

from models import MLP
from datasetloader import DatasetLoader

"""
Variables
"""
# generate parser
parser = argparse.ArgumentParser()
# add argument
parser.add_argument("-b", "--batch_size", type=int, default=100)
parser.add_argument("-e", "--epochs", type=int, default=15)
# analyze argument
args = parser.parse_args()
batch_size = args.batch_size
num_epochs = args.epochs
# set list
losses = []
val_losses = []
accs = []
val_accs = []
log_epoch = []
r = []
# set learning rate
lr = 0.001

# set path
path_dict = {
    "graph": "./results_{}epochs_{}lr/graph".format(num_epochs, lr),
    "log": "./results_{}epochs_{}lr/log".format(num_epochs, lr)
}

# use cuda
device = "cuda" if torch.cuda.is_available() else "cpu"

# get time
dt = datetime.datetime.now()
year = dt.year
month = dt.month
day = dt.day

# start time
start = time.time()

"""
Load data
"""


def load_data():
    log_epoch.append('##################start#####################')

    print('### Loading Data.....###')
    log_epoch.append('### Loading Data.....###')
    # get train_dataset:MNIST
    train_dataloader, num_batches = DatasetLoader.MNIST(num_batches=batch_size)
    # get test_dataset:MNIST
    validation_dataloader, num_batches = DatasetLoader.MNIST(
        train=False,
        num_batches=batch_size)
    print('### Finish Loading ###')
    log_epoch.append('### Finish Loading ###')
    return train_dataloader, validation_dataloader


"""
Define Networks
"""
model = MLP()
# use cuda
if device == "cuda":
    model.to(device)


"""
Define Criterion and Optimizer
"""
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=lr)


def train_and_validate(train_dataloader, validation_dataloader):
    print('--------------------------------------------------')
    log_epoch.append('--------------------------------------------------')
    for epoch in range(num_epochs):
        # learning phase
        running_loss = 0.0
        running_acc = 0.0
        for imgs, labels in train_dataloader:
            imgs = imgs.view(batch_size, -1)
            # use cuda
            if device == "cuda":
                imgs = imgs.to(device)
                labels = labels.to(device)
            optimizer.zero_grad()
            output = model(imgs)
            loss = criterion(output, labels)
            running_loss += loss.item()
            pred = torch.argmax(output, dim=1)
            running_acc += torch.mean(pred.eq(labels).float())
            loss.backward()
            optimizer.step()
        running_loss /= len(train_dataloader)
        running_acc /= len(train_dataloader)
        losses.append(running_loss)
        accs.append(running_acc)

        # validation phase
        val_running_loss = 0.0
        val_running_acc = 0.0
        for val_imgs, val_labels in validation_dataloader:
            val_imgs = val_imgs.view(batch_size, -1)
            # use cuda
            if device == "cuda":
                val_imgs = val_imgs.to(device)
                val_labels = val_labels.to(device)
            val_output = model(val_imgs)
            val_loss = criterion(val_output, val_labels)
            val_running_loss += val_loss.item()
            val_pred = torch.argmax(val_output, dim=1)
            val_running_acc += torch.mean(val_pred.eq(val_labels).float())
        val_running_loss /= len(validation_dataloader)
        val_running_acc /= len(validation_dataloader)
        val_losses.append(val_running_loss)
        val_accs.append(val_running_acc)
        print("epoch: {}, loss: {}, acc: {}".format(epoch, running_loss,
              running_acc))
        log_epoch.append("epoch: {}, loss: {}, acc: {}".format(epoch,
                         running_loss, running_acc))
        print("epoch: {}, val_loss: {}, val_acc: {}".format(epoch,
              val_running_loss, val_running_acc))
        log_epoch.append("epoch: {}, val_loss: {}, val_acc: {}".format(epoch,
                         val_running_loss, val_running_acc))
        print('--------------------------------------------------')
        log_epoch.append('--------------------------------------------------')
    # end time
    end = time.time()
    print('Training time: {} min'.format((end - start)/60))
    log_epoch.append('Training time: {} min'.format((end - start)/60))
    log_epoch.append('##################end#####################')
    return losses, accs, val_losses, val_accs


if __name__ == '__main__':
    for path in path_dict.values():
        if not os.path.exists(path):
            os.makedirs(path)
    # generate log
    log_file = open(path_dict["log"] +
                    "/{}-{}-{}.txt".format(year, month, day), "a")
    # get dataset
    train_dataloader, validation_dataloader = load_data()
    # train and validate
    losses, accs, val_losses, val_accs = train_and_validate(
        train_dataloader,
        validation_dataloader)
    # write log
    r = '\n'.join(log_epoch)
    log_file.writelines(r)

    # plot learning result of loss
    plt.figure(1)
    plt.plot(range(num_epochs), losses)
    plt.title("Loss of Learning phase")
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.savefig(path_dict["graph"] + '/loss.png')
    # plot learning result of accuracy
    plt.figure(2)
    plt.plot(range(num_epochs), accs)
    plt.title("Accuracy of Learning phase")
    plt.xlabel("epoch")
    plt.ylabel("accuracy")
    plt.savefig(path_dict["graph"] + '/accuracy.png')
    # plot validation result of loss
    plt.figure(3)
    plt.plot(range(num_epochs), val_losses)
    plt.title("Loss of Validation phase")
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.savefig(path_dict["graph"] + '/val_loss.png')
    # plot validation result of accuracy
    plt.figure(4)
    plt.plot(range(num_epochs), val_accs)
    plt.title("Accuracy of Validation phase")
    plt.xlabel("epoch")
    plt.ylabel("accuracy")
    plt.savefig(path_dict["graph"] + '/val_accuracy.png')
