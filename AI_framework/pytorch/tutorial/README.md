# pytorch tutorial

Tutorial-level code to understand pytorch.

## Multilayer Perceptron(MLP)

Run Multilayer Perceptron(MLP) with pytorch.

```shell
# Install dependencies from ``requirements.txt``
sudo pip3 install -r requirements.txt

# Run Multilayer Perceptron(MLP)
python3 mlp.py
```

Note: When using conda, it may conflict with pip depending on the installation status of conda.

### Result Example
```shell
cd ./results_15epochs_0.001lr/graph

# Check Graphs
qlmanage -p val_loss.png
qlmanage -p val_accuracy.png

cd ../log

# Check log
less 2021-8-14.txt
```
