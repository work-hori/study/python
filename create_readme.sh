#!/bin/bash

# variable
PROJECT_NAME="python"
PROJECT_URL="work-hori/study/python.git"
DIR_TYPE=`tree`
AUTHOR="Kyosuke Hori"
AUTHOR_EMAIL="kyosuke1117sucre@gmail.com"

# main
echo "create README.md..."

cat <<EOF > README.md
# $PROJECT_NAME

## Description
A project that introduces programs about $PROJECT_NAME.

## Clone
### SSH

`git clone git@gitlab.com:$PROJECT_URL`

### HTTPS

`git clone https://gitlab.com/$PROJECT_URL`

## Directory tree

<pre>
$DIR_TYPE
</pre>

## Author
### Name
$AUTHOR

### Email
$AUTHOR_EMAIL

EOF

echo "create done."
echo "check README.md"
cat README.md

exit 0